
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl import cell

def conversion_card(card_num):
    try:
        tmp =hex(card_num)#輸入數值
        str_tmp=str(tmp)#輸出字串
        
    except :
        print('error!')
        return("None")
    
    str_newtmp=""
    for i in range(1,5,1) :
        #print (str_tmp[2*i:(2*i+2)])
        #if str_newtmp
        #print (len(str_tmp[2*i:(2*i+2)]))
        #print (str_tmp[2*i:(2*i+2)])
        if len(str_tmp[2*i:(2*i+2)]) < 2 :     
            str_newtmp =str_tmp[2*i:(2*i+2)]+'0'+str_newtmp
        else :
            str_newtmp =str_tmp[2*i:(2*i+2)]+str_newtmp
    str_newtmp.upper()
    #print(str_newtmp.upper())
    return(str_newtmp.upper())

read_row=2  #資料輸入垂直位置
write_row=5 #資料輸出垂直位置
now_row=1   #起步

#wb = Workbook()
wb = load_workbook(filename="data.xlsx")

sheets = wb.get_sheet_names()   # 获取所有表格(worksheet)的名字
sheet0 = sheets[0]  # 第一个表格的名称
ws = wb.get_sheet_by_name(sheet0)

for row in ws.rows:
    #print (ws.cell(row=now_row,column=3).value)
    x=ws.cell(row=now_row,column=3).value
    #print (conversion_card(x))
    ws.cell(row = now_row, column = 5).value = conversion_card(x)#寫入資料
    now_row=now_row+1

wb.save('data.xlsx')


